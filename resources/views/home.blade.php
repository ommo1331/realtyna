@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                 <!-- User Information -->
                <div class="panel-body">
                    <ul class="list-group">
                        <li class="list-group-item">Name: &nbsp; {{$user->name}}</li>
                        <li class="list-group-item">Email: &nbsp; {{$user->email}}</li>
                        <li class="list-group-item">Role: &nbsp; {{$user->role}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
