@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row">
        <div id="containerdiv", class="col-md-10 col-md-offset-1"></div>
    </div>
</div>
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/postHtmpToAppend.js"></script>
        <script type="text/javascript">    
            $(document).ready(function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                   method: "POST",
                   url: "{{url('ajaxAllPosts')}}"
                }).done(function(data) {
                   $.each(data, function(index, value){

                        //Lets create html to append
                        var htmlToAppend = makeHtml(value);       
                        $('#containerdiv').append(htmlToAppend);

                        // When Read button is clicked
                        $("#readpost_"+value['id']).on('click', function(){          
                            var url = "/realtyna/public/post/" + value['id'];
                            window.location.href = url; 
                        });
                    });
                });
            });
        </script>
@endsection
