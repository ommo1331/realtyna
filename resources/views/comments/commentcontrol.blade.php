@extends('layouts.app')

@section('content')

@foreach($allcomms as $comment)

	<div class="container">
		<div class="row">
			<div id="containerdiv", class="col-md-10 col-md-offset-1">
			    <div class='panel panel-default'>

				    <div class='panel-heading', style='background-color:#A9CCE3;  '>{{$comment->author}} </div>

				    <div class='panel-body', style='background-color: #EAF2F8;'>{{$comment->content}}</div>

				    <div class='panel-body', style='background-color: #EAF2F8;'><hr>
				    <span class='glyphicon glyphicon-time'></span>{{$comment->created_at}}</div>  

		            <div class='panel-body', style='background-color: #EAF2F8;'>Permission: {{$comment->permission}}</div>

		            <div class='panel-body', style='background-color: #EAF2F8;'>
		            
		            <!-- Comment Delete button  -->
						{!! Form::open(['method' => 'DELETE', 'route' => ['commentresource.destroy', $comment->id]]) !!}
						{!! Form::submit('Delete',  array('class' => 'btn btn-default'))  !!}
						{!! Form::close() !!}
					</div>
		      	</div>
		    </div>
		</div>
	</div>

@endforeach

@endsection


