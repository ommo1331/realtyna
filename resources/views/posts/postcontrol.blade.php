@extends('layouts.app')

@section('content')


@foreach($allposts as $post)
	<div class="container">
	    <div class="row">
			<div id="containerdiv", class="col-md-10 col-md-offset-1">
			    <div class='panel panel-default'>
			        <div class='panel-heading', style='background-color:#A9DFBF;'>{{$post->headline}}</div>
			        	<div class='panel-body', style='background-color:#E9F7EF;'>{{$post->content}}<hr>Author: {{$post->author}}<hr><span class='glyphicon glyphicon-time'></span> {{$post->created_at}}<hr>Permission: {{$post->permission}}<hr>

			        		<!-- We have three form to Edit, Delete or Approve/Disapprove Post -->

				        	{!! Form::open(['method' => 'GET', 'route' => ['postresource.edit', $post->id]]) !!}
							{!! Form::submit('Edit',  array('class' => 'btn btn-default', 'style'=>'float: left;'))  !!}
							{!! Form::close() !!}

							{!! Form::open(['method' => 'DELETE', 'route' => ['postresource.destroy', $post->id]]) !!}
							{!! Form::submit('Delete',  array('class' => 'btn btn-default', 'style'=>'float: left;'))  !!}
							{!! Form::close() !!}

							{!! Form::open(['method' => 'POST', 'route' => ['permission', $post->id]]) !!}
							{!! Form::submit('Approve/Disapprove',  array('class' => 'btn btn-default', 'style'=>'float: left;'))  !!}
							{!! Form::close() !!}
						
			        	</div>
		         	</div>
			    </div>
		    </div>
		</div>

		@endforeach
@endsection


