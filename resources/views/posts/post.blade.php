@extends('layouts.app')

@section('content')


<script src="../js/jquery-3.2.1.min.js"></script>
<div class="container">
    <div id="containerdiv", class="col-md-10 col-md-offset-1">

      		<!-- Post Headline -->
	       	<div class='panel-heading', style='background-color:#A9DFBF;'>
	       		<h2>{{$post->headline}}</h2>
	       	</div>  

	       	<!-- Post Body -->
	       	<div class='panel-body', style='background-color:#E9F7EF;'>

	       		<h4>{{$post->content}}</h4><hr>
	       		<h4>Author:&nbsp {{$post->author}}</h4><hr>
	       		<h4><span class='glyphicon glyphicon-time'></span>&nbsp {{$post->created_at}}</h4><hr>
	       		<h4>Comments:</h4><br>

	       		<!-- Logged in user is able to leave comment -->
	        	@if(\Auth::check())
                   	<textarea id="comm-txt-area", class="form-control", rows='4', cols='80', id = 'txar'></textarea><br>
                   	<button id="commentButton", class='btn btn-primary'>Comment</button><br><br>
                   	
                   	<!-- Lets use ajax to add comment -->
                   	<script type="text/javascript">
                   		$('#comm-txt-area').prop('placeholder', 'Leave your comment here..');
                   		 $(document).ready(function(){                  		 	
                   		 	$('#commentButton').on('click', function(){
                   		 		var text = $('#comm-txt-area').val();
                   		 		$.ajaxSetup({
					                headers: {
				                      	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				                    }
				                });
                   		 		$.ajax({
				                   method: "POST",
				                   url: "{{url('addcomment')}}",
				                   data: {content:text, post_id:<?php echo $post->id?>}
				                }).done(function(data) {
				                	location.reload();
				                });
                   		 	});
                   		 });
                   	</script>
                @endif
                   
                <!-- Show Comments -->
                @foreach($post->comments as $comment)
                    <div class='panel panel-default'>
	     	            <div class='panel-heading', style='background-color:#A9CCE3;  '>{{$comment->author}} </div>
	            		<div class='panel-body', style='background-color: #EAF2F8;'>{{$comment->content}}</div>
		              	<div class='panel-body', style='background-color: #EAF2F8;'><hr><span class='glyphicon glyphicon-time'></span>{{$comment->created_at}}</div>		
	                </div>                   			
               	@endforeach            	
                    	
        </div>
    </div>
</div>


@endsection
