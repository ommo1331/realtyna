@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add post</div>
                <div class="panel-body">
                
                	<!-- Form to edit post -->
               		{!!Form::model($post, array('method'=>'PUT','route' => array('postresource.update', $post->id))) !!}

					<div class="form-group">
						{!!  Form::label('headline', 'Headline') !!}
						{!!  Form::text('headline', null, array('class' => 'form-control'))  !!}

					</div><br>

					<div class="form-group">
						{!!  Form::label('content', 'Content') !!}
						{!!  Form::textarea('content', null, array('class' => 'form-control'))  !!}

					</div><br>

					<div class="form-group">
						{!!  Form::label('author', 'Author') !!}
						{!!  Form::text('author', null, array('class' => 'form-control'))  !!}
					</div>

					{!! Form::token() !!}
					<br>
					{!! Form::submit(null, array('class' => 'btn btn-default'))  !!}
					{!! Form::close() !!}
              
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

