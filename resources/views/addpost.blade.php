@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add post</div>
                <div class="panel-body">
                 <!-- Form To Add Post -->
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/createpost') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Headline</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="headline" value="{{ old('name') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Content</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="content" value="{{ old('name') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
