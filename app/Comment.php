<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    

    protected $fillable = [
        'user_id', 'post_id', 'content', 'permission', 'author',
    ];



     /**
     * Many comments belongs to one user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }



     /**
     * Many comments belongs to one post
     */
     public function post()
    {
        return $this->belongsTo('App\Post');
    }
}
