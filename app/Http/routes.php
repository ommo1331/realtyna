<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});


Route::auth();

Route::get('/home', 'HomeController@index');
Route::post('ajaxAllPosts','PostController@ajaxIndex');
Route::get('/post/{id}', 'PostController@readPost');


Route::group(['middleware' => 'auth'], function() {

	Route::get('/postform', 'PostController@create');
	Route::post('/createpost', 'PostController@store');
	Route::post('/addcomment', 'CommentController@store');

});


Route::group(['middleware' => 'admin'], function() {

	Route::get('/postcontrol', 'PostController@index')->name('postcontrol');
	Route::resource('postresource', 'PostController',['only' => ['edit', 'update', 'destroy']]);
	Route::post('/permission/{id}', 'PostController@permission')->name("permission");

	Route::get('/commentcontrol', 'CommentController@index')->name('commentcontrol');
	Route::resource('commentresource', 'CommentController',['only' => ['destroy']]);

});

