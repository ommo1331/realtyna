<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allposts = Post::all();
        return view('posts.postcontrol',compact('allposts'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxIndex()
    {
        $posts = Post::where('permission', 'yes')->get();
        return $posts;
    }

    /**
     * Display a source.
     *
     * @return \Illuminate\Http\Response
     */
    public function readPost($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.post',compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('posts.addpost');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post(); 
        $post->user_id = \Auth::user()->id;
        $post->headline = $request['headline'];
        $post->content = $request['content'];
        $post->author = \Auth::user()->name;
        if(\Auth::user()->role == 'admin')
            $post->permission = 'yes';
        else
            $post->permission = 'no';
        $post->save();
        return back();
    }
  
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.editpost',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->headline = $request->headline;
        $post->content = $request->content;
        $post->author = $request->author;
        $post->save();
        $allposts = Post::all();
        return redirect()->route('postcontrol');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id)->delete();
        return redirect()->route('postcontrol');  
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function permission($id){
        $post = Post::findOrFail($id);
        if($post->permission == "no"){
            $post->permission = "yes";
            $post->save();
        }else{
             $post->permission = "no";
            $post->save();
        }
        return redirect()->route('postcontrol');
    }
}
