<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'headline', 'content', 'author', 'permission',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     /**
     * Many posts belongs to one user
     */
     public function user()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * One posts has many comments
     */
      public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
