 function makeHtml(value){
                 var htmlToAppend = "";
                        htmlToAppend += "<div class='panel panel-default'>";
                        htmlToAppend += "<div class='panel-heading', style='background-color:#A9DFBF;'>";
                        htmlToAppend += value['headline'];
                        htmlToAppend += "</div>";        
                        htmlToAppend += "<div class='panel-body', style='background-color:#E9F7EF;'>";
                        htmlToAppend += value['content'];
                        htmlToAppend += "<hr>";
                        htmlToAppend += "Author: " + value['author'];
                        htmlToAppend += "<hr>";
                        htmlToAppend += "<span class='glyphicon glyphicon-time'></span> " + value['created_at'];
                        htmlToAppend += "<hr>";
                        htmlToAppend += "<button id='readpost_"+value['id']+"', class='btn btn-primary'>Read</button>";
                        htmlToAppend += "</div>";
                        htmlToAppend += "</div>";
                return htmlToAppend;
            }
            